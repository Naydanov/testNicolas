(function($){
'use strict';
  jQuery.fn.generateSnow = function(option){
    var id = option.id;
    var count = option.count;
    var countImg = option.img.length;
    var delayStart = option.delay.start;
    var delayEnd = option.delay.end;
    var ignore = option.ignore;

    function startSnow(){
        var nameImg = Math.floor(Math.random()*countImg) + 1;
        var positionX = Math.floor(Math.random()*98) + 1;
        var step = Math.floor(Math.random()*40) - 10;
        var endX = positionX + step;

        var img="<img id='snow_" + id + "' style='left:" + positionX +
        "%; top:-30%; position:fixed;' src='img/" + nameImg + ".png'>";
    
        $('body').append(img);
        moveShow(id, endX);
        id++;
        if(id <= count){
            setTimeout(function(){
                startSnow()
            }, delayStart); 
        }      
    }

    var moveShow = function(id, endX){
        $("#snow_" + id).animate({top:"120%", left:"" + endX + "%"}, delayEnd, function(){
            $("#snow_" + id).empty().remove();
        });
    }
    
    var makeIgnoreElement = function(){
        ignore.forEach(function(element){
            $(element).css("z-index", "1000");
        }); 
    }

     window.onload=function(){
         makeIgnoreElement();
         startSnow();
    }
  };
})(jQuery);