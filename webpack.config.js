const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  watch: true,
  entry: {
    app: './src/index.jsx'
  },

  output: {
    path: path.resolve(__dirname, 'build'),
    filename: "bundle.js",
    publicPath: '/',
  },

  resolve: {
    modules: ['src', 'node_modules'],
    extensions: [".js", ".json", ".jsx", ".css"],
  },

  module: {
    rules: [
      { test: /\.css$/, use: 'css-loader' },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
      { test: /\.styl$/, 
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'stylus-loader',
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|ttf|woff|woff2)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: 'img/[name].[ext]',
              limit: 10000,
            },
          },
        ],
      },
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'index.html',
    }),
  ],

  devServer: {
    contentBase: path.resolve('build'),
    host: 'localhost',
    port: 8080,
    historyApiFallback: true,
  }
};