import React, { Component, PropTypes } from 'react';

import block from 'bem-cn';
import './App.styl'

import Header from '../section/Header';
import Menu from '../section/Menu'
import AboutUs from '../section/AboutUs'
import Demarcator from '../shared/Demarcator'
import Services from '../section/Services'
import Reviews from '../section/Reviews'
import Contact from '../section/Contact'; 

class Button extends Component {

constructor(props) {
super(props);
this.b = block('app');
}

render() {
    const b = this.b;
        return (
            <div className={b}>
                <Header />
                <Menu />
                <AboutUs />
                <Demarcator />
                <Services />
                <Demarcator />
                <Reviews />
                <Demarcator />
                <Contact />
            </div>
        );
    }
}

export default Button;