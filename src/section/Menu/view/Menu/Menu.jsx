import React, { Component, PropTypes } from 'react';

import block from 'bem-cn';
import './Menu.styl';

class Menu extends Component {

constructor(props) {
super(props);
}

render() {
    const b = block('menu');
        return (
            <section className={b}>
                <a className={b('link')} href="#aboutUs">О нас</a>
                <a className={b('link')} href="#services">Услуги</a>
                <a className={b('link')} href="#review">Отзывы</a>
                <a className={b('link')} href="#contact">Контакты</a>
                <a className={b('link', {triangle: true })}></a>
            </section>
        );
    }
}

export default Menu;