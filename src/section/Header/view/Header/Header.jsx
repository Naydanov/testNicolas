import React, { Component, PropTypes } from 'react';

import block from 'bem-cn';
import LogoImg from '../img/logo.png';
import './Header.styl';
import Article from '../Article/Article';
import Contact from '../Contact/Contact';
import Slider from '../Slider/Slider';

class Header extends Component {

constructor(props) {
super(props);
}

render() {
    const b = block('header');
        return (
            <section className={b}>
                <div className={b('wrapper', { position: 'left' })}>
                    <img src={LogoImg} className={b('logo')} />
                </div>
                <div className={b('wrapper', { position: 'center' })}>
                    <Article />
                    <Slider />
                </div>
                <div className={b('wrapper', { position: 'right' })}>
                    <Contact />
                </div>
            </section>
        );
    }
}

export default Header;