import React, { Component, PropTypes } from 'react';

import block from 'bem-cn';
import './Slider.styl'

class Slider extends Component {

constructor(props) {
super(props);
}

render() {
    const b = block('slider');
        return (
            <div className={b}>
                <div className={b('circles')}>
                    {/*<svg viewBox="0 0 5 5" version="1.1">
                        <circle cx="60" cy="60" r="1"/>
                    </svg>*/}
                </div>
            </div>
        );
    }
}

export default Slider;