import React, { Component, PropTypes } from 'react';

import block from 'bem-cn';
import Article from '../Article/Article';
import './AboutUs.styl';

class AboutUs extends Component {

constructor(props) {
super(props);
}

render() {
    const b = block('about-us');
        return (
            <section className={b}>
                <Article />
                <div className={b('description')}>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius tempore aut, inventore dolor harum veniam ipsa illum explicabo ipsum assumenda, at corporis veritatis quas error dolorem voluptatem. Inventore, eligendi, impedit.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui nam facere magnam maiores adipisci quidem voluptas obcaecati vero eaque consectetur, recusandae ex hic expedita consequuntur facilis modi ab. Vel, illum.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut doloremque eius molestiae quibusdam cumque beatae, unde soluta repellat minima, voluptatum maiores ipsum! Sapiente, sequi quasi! Architecto voluptates, at quidem. Expedita!
                    </p>
                </div>
            </section>
        );
    }
}

export default AboutUs;