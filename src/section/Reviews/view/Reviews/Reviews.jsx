import React, { Component, PropTypes } from 'react';

import block from 'bem-cn';
import './Reviews.styl';
import ReviewItem from '../ReviewItem/ReviewItem';
import Article from '../Article/Article'
import FormReview from '../FormReview/FormReview'

class Reviews extends Component {

constructor(props) {
super(props);
}

render() {
    const b = block('reviews');
        return (
            <section className={b}>
                <Article />
                <ReviewItem />
                <ReviewItem />
                <FormReview />
            </section>
        );
    }
}

export default Reviews;