import React, { Component, PropTypes } from 'react';

import block from 'bem-cn';
import './FormReview.styl';

class FormReview extends Component {

constructor(props) {
super(props);
}

render() {
    const b = block('form-review');
        return (
            <form className={b}>
                <h2 className={b('caption')}>Оставить отзыв</h2>
                <div className={b('group-input')}>
                    <input className={b('group-input','name')} placeholder="ФИО"></input>
                    <input className ={b('group-input','mail')} placeholder="E-mail"></input>
                    <textarea className ={b('group-input','comment')}></textarea>
                </div>
                <div className={b('group-button')}>
                    <div className={b('checkbox')}>
                        <input type="checkbox"></input>
                        <input type="checkbox" checked></input>
                    </div>
                    <div className={b('radio')}>
                        <input type="radio"></input>
                        <input type="radio" checked></input>
                    </div>
                </div>
                <div className={b("sendComment")}>
                    <button>Отправить</button>
                </div>

            </form>
        );
    }
}

export default FormReview;