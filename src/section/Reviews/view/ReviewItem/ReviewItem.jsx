import React, { Component, PropTypes } from 'react';

import block from 'bem-cn';
import './ReviewItem.styl';

class ReviewItem extends Component {

constructor(props) {
super(props);
}

render() {
    const b = block('review-item');
        return (
            <div className={b}>
                <div className={b('name')}><strong>Олег А.</strong></div>
                <div className={b('date')}> 10.05.2016</div>
                <div className={b('text')}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat quae, consequatur. Reiciendis accusantium omnis harum et nisi laudantium repudiandae placeat officia eveniet, tenetur, iure deserunt! Blanditiis, quod repudiandae quos ullam.
                </div>
            </div>
        );
    }
}

export default ReviewItem;