import React, { Component, PropTypes } from 'react';

import block from 'bem-cn';
import './Contact.styl';
import Article from '../Article/Article';

class Contact extends Component {

constructor(props) {
super(props);
}

render() {
    const b = block('contacts');
        return (
            <section className={b}>
                <Article />
            </section>
        );
    }
}

export default Contact;