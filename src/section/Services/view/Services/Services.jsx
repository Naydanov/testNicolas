import React, { Component, PropTypes } from 'react';

import block from 'bem-cn';
import './Services.styl';
import Article from '../Article/Article'

class Services extends Component {

constructor(props) {
super(props);
}

render() {
    const b = block('services');
        return (
            <div className={b}>
                <Article />
                <a href="#" className={b('item',{repairs: true})}> Ремонт двигателя</a>
                <a href="#" className={b('item', {mounting: true})}>Шиномонтаж</a>
                <a href="#" className={b('item', {repairSuspension: true})}>Ремонт подвески</a>
                <a href="#" className={b('item', {repairsBox: true})}>Ремонт коробки</a>
                <a href="#" className={b('item', {changeOil: true})}>Замена масла</a>
                <a href="#" className={b('item', {bodyRepair: true})}>Кузовной ремонт</a>
                <a href="#" className={b('item', {carWash: true})}>Автомойка</a>
            </div>
        );
    }
}

export default Services;