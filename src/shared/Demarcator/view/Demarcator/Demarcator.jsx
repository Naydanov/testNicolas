import React, { Component, PropTypes } from 'react';

import block from 'bem-cn';
import './Demarcator.styl';

class Demarcator extends Component {

constructor(props) {
super(props);
}

render() {
    const b = block('demarcator');
        return (
            <div className={b}>
                <hr />
            </div>
        );
    }
}

export default Demarcator;